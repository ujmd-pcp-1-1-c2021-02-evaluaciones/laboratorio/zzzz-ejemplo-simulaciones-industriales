﻿
namespace Simulaciones_Industriales
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelHome = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPuertaAutoclave = new System.Windows.Forms.Button();
            this.btnNivelTanque = new System.Windows.Forms.Button();
            this.btnTemperaturaHorno = new System.Windows.Forms.Button();
            this.ucHorno1 = new Simulaciones_Industriales.ucHorno();
            this.ucTanque1 = new Simulaciones_Industriales.ucTanque();
            this.ucPuertaAutoclave1 = new Simulaciones_Industriales.ucPuertaAutoclave();
            this.rbtnInicio = new System.Windows.Forms.RadioButton();
            this.rbtnPuertaAutoclave = new System.Windows.Forms.RadioButton();
            this.rbtnNivelTanque = new System.Windows.Forms.RadioButton();
            this.rbtnTemperaturaHorno = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panelHome.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(74)))), ((int)(((byte)(14)))));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.rbtnInicio, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.rbtnPuertaAutoclave, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.rbtnNivelTanque, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.rbtnTemperaturaHorno, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 490);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.LemonChiffon;
            this.pictureBox1.Image = global::Simulaciones_Industriales.Properties.Resources.Embotellado_garrafas;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 144);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LemonChiffon;
            this.panel1.Controls.Add(this.panelHome);
            this.panel1.Controls.Add(this.ucHorno1);
            this.panel1.Controls.Add(this.ucTanque1);
            this.panel1.Controls.Add(this.ucPuertaAutoclave1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(153, 3);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 6);
            this.panel1.Size = new System.Drawing.Size(644, 484);
            this.panel1.TabIndex = 1;
            // 
            // panelHome
            // 
            this.panelHome.Controls.Add(this.tableLayoutPanel2);
            this.panelHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHome.Location = new System.Drawing.Point(0, 0);
            this.panelHome.Name = "panelHome";
            this.panelHome.Size = new System.Drawing.Size(644, 484);
            this.panelHome.TabIndex = 3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.pictureBox2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 69.00826F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30.99174F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(644, 484);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.LemonChiffon;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Image = global::Simulaciones_Industriales.Properties.Resources.Embotellado_garrafas;
            this.pictureBox2.Location = new System.Drawing.Point(3, 57);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(638, 273);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.btnPuertaAutoclave, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnNivelTanque, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnTemperaturaHorno, 3, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 336);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(638, 58);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // btnPuertaAutoclave
            // 
            this.btnPuertaAutoclave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(74)))), ((int)(((byte)(14)))));
            this.btnPuertaAutoclave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPuertaAutoclave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(37)))), ((int)(((byte)(7)))));
            this.btnPuertaAutoclave.FlatAppearance.BorderSize = 2;
            this.btnPuertaAutoclave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Goldenrod;
            this.btnPuertaAutoclave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPuertaAutoclave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnPuertaAutoclave.ForeColor = System.Drawing.Color.LemonChiffon;
            this.btnPuertaAutoclave.Image = global::Simulaciones_Industriales.Properties.Resources.door;
            this.btnPuertaAutoclave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPuertaAutoclave.Location = new System.Drawing.Point(97, 3);
            this.btnPuertaAutoclave.Name = "btnPuertaAutoclave";
            this.btnPuertaAutoclave.Size = new System.Drawing.Size(144, 52);
            this.btnPuertaAutoclave.TabIndex = 0;
            this.btnPuertaAutoclave.Text = "          Puerta de\r\n          autoclave\r\n";
            this.btnPuertaAutoclave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPuertaAutoclave.UseVisualStyleBackColor = false;
            this.btnPuertaAutoclave.Click += new System.EventHandler(this.btnPuertaAutoclave_Click);
            // 
            // btnNivelTanque
            // 
            this.btnNivelTanque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(74)))), ((int)(((byte)(14)))));
            this.btnNivelTanque.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNivelTanque.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(37)))), ((int)(((byte)(7)))));
            this.btnNivelTanque.FlatAppearance.BorderSize = 2;
            this.btnNivelTanque.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Goldenrod;
            this.btnNivelTanque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNivelTanque.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnNivelTanque.ForeColor = System.Drawing.Color.LemonChiffon;
            this.btnNivelTanque.Image = global::Simulaciones_Industriales.Properties.Resources.tank;
            this.btnNivelTanque.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNivelTanque.Location = new System.Drawing.Point(247, 3);
            this.btnNivelTanque.Name = "btnNivelTanque";
            this.btnNivelTanque.Size = new System.Drawing.Size(144, 52);
            this.btnNivelTanque.TabIndex = 0;
            this.btnNivelTanque.Text = "          Nivel de tanque";
            this.btnNivelTanque.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNivelTanque.UseVisualStyleBackColor = false;
            this.btnNivelTanque.Click += new System.EventHandler(this.btnNivelTanque_Click);
            // 
            // btnTemperaturaHorno
            // 
            this.btnTemperaturaHorno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(74)))), ((int)(((byte)(14)))));
            this.btnTemperaturaHorno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTemperaturaHorno.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(37)))), ((int)(((byte)(7)))));
            this.btnTemperaturaHorno.FlatAppearance.BorderSize = 2;
            this.btnTemperaturaHorno.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Goldenrod;
            this.btnTemperaturaHorno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTemperaturaHorno.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnTemperaturaHorno.ForeColor = System.Drawing.Color.LemonChiffon;
            this.btnTemperaturaHorno.Image = global::Simulaciones_Industriales.Properties.Resources.furnace;
            this.btnTemperaturaHorno.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTemperaturaHorno.Location = new System.Drawing.Point(397, 3);
            this.btnTemperaturaHorno.Name = "btnTemperaturaHorno";
            this.btnTemperaturaHorno.Size = new System.Drawing.Size(144, 52);
            this.btnTemperaturaHorno.TabIndex = 0;
            this.btnTemperaturaHorno.Text = "          Temperatura\r\n          de horno\r\n";
            this.btnTemperaturaHorno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTemperaturaHorno.UseVisualStyleBackColor = false;
            this.btnTemperaturaHorno.Click += new System.EventHandler(this.btnTemperaturaHorno_Click);
            // 
            // ucHorno1
            // 
            this.ucHorno1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucHorno1.Location = new System.Drawing.Point(0, 0);
            this.ucHorno1.Name = "ucHorno1";
            this.ucHorno1.Size = new System.Drawing.Size(644, 484);
            this.ucHorno1.TabIndex = 2;
            this.ucHorno1.Visible = false;
            // 
            // ucTanque1
            // 
            this.ucTanque1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTanque1.Location = new System.Drawing.Point(0, 0);
            this.ucTanque1.Name = "ucTanque1";
            this.ucTanque1.Size = new System.Drawing.Size(644, 484);
            this.ucTanque1.TabIndex = 1;
            this.ucTanque1.Visible = false;
            // 
            // ucPuertaAutoclave1
            // 
            this.ucPuertaAutoclave1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPuertaAutoclave1.Location = new System.Drawing.Point(0, 0);
            this.ucPuertaAutoclave1.Name = "ucPuertaAutoclave1";
            this.ucPuertaAutoclave1.Size = new System.Drawing.Size(644, 484);
            this.ucPuertaAutoclave1.TabIndex = 0;
            this.ucPuertaAutoclave1.Visible = false;
            // 
            // rbtnInicio
            // 
            this.rbtnInicio.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnInicio.Checked = true;
            this.rbtnInicio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnInicio.FlatAppearance.BorderColor = System.Drawing.Color.LemonChiffon;
            this.rbtnInicio.FlatAppearance.BorderSize = 2;
            this.rbtnInicio.FlatAppearance.CheckedBackColor = System.Drawing.Color.Goldenrod;
            this.rbtnInicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnInicio.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnInicio.ForeColor = System.Drawing.Color.LemonChiffon;
            this.rbtnInicio.Image = global::Simulaciones_Industriales.Properties.Resources.home;
            this.rbtnInicio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtnInicio.Location = new System.Drawing.Point(3, 153);
            this.rbtnInicio.Name = "rbtnInicio";
            this.rbtnInicio.Size = new System.Drawing.Size(144, 44);
            this.rbtnInicio.TabIndex = 2;
            this.rbtnInicio.TabStop = true;
            this.rbtnInicio.Text = "          Inicio";
            this.rbtnInicio.UseVisualStyleBackColor = true;
            this.rbtnInicio.CheckedChanged += new System.EventHandler(this.rbtnInicio_CheckedChanged);
            // 
            // rbtnPuertaAutoclave
            // 
            this.rbtnPuertaAutoclave.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnPuertaAutoclave.FlatAppearance.BorderColor = System.Drawing.Color.LemonChiffon;
            this.rbtnPuertaAutoclave.FlatAppearance.BorderSize = 2;
            this.rbtnPuertaAutoclave.FlatAppearance.CheckedBackColor = System.Drawing.Color.Goldenrod;
            this.rbtnPuertaAutoclave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnPuertaAutoclave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnPuertaAutoclave.ForeColor = System.Drawing.Color.LemonChiffon;
            this.rbtnPuertaAutoclave.Image = global::Simulaciones_Industriales.Properties.Resources.door;
            this.rbtnPuertaAutoclave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtnPuertaAutoclave.Location = new System.Drawing.Point(3, 203);
            this.rbtnPuertaAutoclave.Name = "rbtnPuertaAutoclave";
            this.rbtnPuertaAutoclave.Size = new System.Drawing.Size(144, 44);
            this.rbtnPuertaAutoclave.TabIndex = 2;
            this.rbtnPuertaAutoclave.Text = "          Puerta de\r\n          autoclave";
            this.rbtnPuertaAutoclave.UseVisualStyleBackColor = true;
            this.rbtnPuertaAutoclave.CheckedChanged += new System.EventHandler(this.rbtnPuertaAutoclave_CheckedChanged);
            // 
            // rbtnNivelTanque
            // 
            this.rbtnNivelTanque.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnNivelTanque.FlatAppearance.BorderColor = System.Drawing.Color.LemonChiffon;
            this.rbtnNivelTanque.FlatAppearance.BorderSize = 2;
            this.rbtnNivelTanque.FlatAppearance.CheckedBackColor = System.Drawing.Color.Goldenrod;
            this.rbtnNivelTanque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnNivelTanque.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnNivelTanque.ForeColor = System.Drawing.Color.LemonChiffon;
            this.rbtnNivelTanque.Image = global::Simulaciones_Industriales.Properties.Resources.tank;
            this.rbtnNivelTanque.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtnNivelTanque.Location = new System.Drawing.Point(3, 253);
            this.rbtnNivelTanque.Name = "rbtnNivelTanque";
            this.rbtnNivelTanque.Size = new System.Drawing.Size(144, 44);
            this.rbtnNivelTanque.TabIndex = 2;
            this.rbtnNivelTanque.Text = "          Nivel de tanque";
            this.rbtnNivelTanque.UseVisualStyleBackColor = true;
            this.rbtnNivelTanque.CheckedChanged += new System.EventHandler(this.rbtnNivelTanque_CheckedChanged);
            // 
            // rbtnTemperaturaHorno
            // 
            this.rbtnTemperaturaHorno.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnTemperaturaHorno.FlatAppearance.BorderColor = System.Drawing.Color.LemonChiffon;
            this.rbtnTemperaturaHorno.FlatAppearance.BorderSize = 2;
            this.rbtnTemperaturaHorno.FlatAppearance.CheckedBackColor = System.Drawing.Color.Goldenrod;
            this.rbtnTemperaturaHorno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTemperaturaHorno.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTemperaturaHorno.ForeColor = System.Drawing.Color.LemonChiffon;
            this.rbtnTemperaturaHorno.Image = global::Simulaciones_Industriales.Properties.Resources.furnace;
            this.rbtnTemperaturaHorno.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtnTemperaturaHorno.Location = new System.Drawing.Point(3, 303);
            this.rbtnTemperaturaHorno.Name = "rbtnTemperaturaHorno";
            this.rbtnTemperaturaHorno.Size = new System.Drawing.Size(144, 44);
            this.rbtnTemperaturaHorno.TabIndex = 2;
            this.rbtnTemperaturaHorno.Text = "          Temperatura\r\n          de horno";
            this.rbtnTemperaturaHorno.UseVisualStyleBackColor = true;
            this.rbtnTemperaturaHorno.CheckedChanged += new System.EventHandler(this.rbtnTemperaturaHorno_CheckedChanged);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 490);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "frmMain";
            this.Text = "Simulaciones de procesos industriales";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panelHome.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbtnInicio;
        private System.Windows.Forms.RadioButton rbtnPuertaAutoclave;
        private System.Windows.Forms.RadioButton rbtnNivelTanque;
        private System.Windows.Forms.RadioButton rbtnTemperaturaHorno;
        private ucPuertaAutoclave ucPuertaAutoclave1;
        private ucTanque ucTanque1;
        private ucHorno ucHorno1;
        private System.Windows.Forms.Panel panelHome;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnPuertaAutoclave;
        private System.Windows.Forms.Button btnNivelTanque;
        private System.Windows.Forms.Button btnTemperaturaHorno;
    }
}

