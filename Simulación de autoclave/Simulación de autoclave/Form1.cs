﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulación_de_autoclave
{
    public partial class frmAutoclave : Form
    {
        public frmAutoclave()
        {
            InitializeComponent();
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            panelPuerta.Height = panelPuerta.Height - 5;
            panelPuerta.Top = panelPuerta.Top + 5;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            panelPuerta.Height = panelPuerta.Height + 5;
            panelPuerta.Top = panelPuerta.Top - 5;
        }
    }
}
