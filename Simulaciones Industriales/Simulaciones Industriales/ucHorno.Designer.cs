﻿
namespace Simulaciones_Industriales
{
    partial class ucHorno
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pbxHornoON = new System.Windows.Forms.PictureBox();
            this.pbxHornoApagado = new System.Windows.Forms.PictureBox();
            this.btnON = new System.Windows.Forms.Button();
            this.btnOFF = new System.Windows.Forms.Button();
            this.pbxTermómetro = new System.Windows.Forms.PictureBox();
            this.panelTermómetro = new System.Windows.Forms.Panel();
            this.panelMercurio = new System.Windows.Forms.FlowLayoutPanel();
            this.tmrON = new System.Windows.Forms.Timer(this.components);
            this.tmrOFF = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.nudTemperatura = new System.Windows.Forms.NumericUpDown();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoApagado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxTermómetro)).BeginInit();
            this.panelTermómetro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTemperatura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxHornoON
            // 
            this.pbxHornoON.Image = global::Simulaciones_Industriales.Properties.Resources.Ahumador_encendido_GIF;
            this.pbxHornoON.Location = new System.Drawing.Point(2, 66);
            this.pbxHornoON.Name = "pbxHornoON";
            this.pbxHornoON.Size = new System.Drawing.Size(150, 150);
            this.pbxHornoON.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxHornoON.TabIndex = 0;
            this.pbxHornoON.TabStop = false;
            this.pbxHornoON.Visible = false;
            // 
            // pbxHornoApagado
            // 
            this.pbxHornoApagado.Image = global::Simulaciones_Industriales.Properties.Resources.Ahumador_apagado;
            this.pbxHornoApagado.Location = new System.Drawing.Point(3, 66);
            this.pbxHornoApagado.Name = "pbxHornoApagado";
            this.pbxHornoApagado.Size = new System.Drawing.Size(150, 150);
            this.pbxHornoApagado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxHornoApagado.TabIndex = 0;
            this.pbxHornoApagado.TabStop = false;
            // 
            // btnON
            // 
            this.btnON.Location = new System.Drawing.Point(12, 222);
            this.btnON.Name = "btnON";
            this.btnON.Size = new System.Drawing.Size(60, 23);
            this.btnON.TabIndex = 1;
            this.btnON.Text = "ON";
            this.btnON.UseVisualStyleBackColor = true;
            this.btnON.Click += new System.EventHandler(this.btnON_Click);
            // 
            // btnOFF
            // 
            this.btnOFF.Location = new System.Drawing.Point(83, 222);
            this.btnOFF.Name = "btnOFF";
            this.btnOFF.Size = new System.Drawing.Size(60, 23);
            this.btnOFF.TabIndex = 1;
            this.btnOFF.Text = "OFF";
            this.btnOFF.UseVisualStyleBackColor = true;
            this.btnOFF.Click += new System.EventHandler(this.btnOFF_Click);
            // 
            // pbxTermómetro
            // 
            this.pbxTermómetro.Image = global::Simulaciones_Industriales.Properties.Resources.Termómetro_26_180_Celcius;
            this.pbxTermómetro.Location = new System.Drawing.Point(160, 5);
            this.pbxTermómetro.Name = "pbxTermómetro";
            this.pbxTermómetro.Size = new System.Drawing.Size(68, 295);
            this.pbxTermómetro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxTermómetro.TabIndex = 2;
            this.pbxTermómetro.TabStop = false;
            // 
            // panelTermómetro
            // 
            this.panelTermómetro.BackColor = System.Drawing.Color.White;
            this.panelTermómetro.Controls.Add(this.panelMercurio);
            this.panelTermómetro.Location = new System.Drawing.Point(183, 36);
            this.panelTermómetro.Name = "panelTermómetro";
            this.panelTermómetro.Size = new System.Drawing.Size(8, 209);
            this.panelTermómetro.TabIndex = 3;
            // 
            // panelMercurio
            // 
            this.panelMercurio.BackColor = System.Drawing.Color.Red;
            this.panelMercurio.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelMercurio.Location = new System.Drawing.Point(0, 160);
            this.panelMercurio.Name = "panelMercurio";
            this.panelMercurio.Size = new System.Drawing.Size(8, 49);
            this.panelMercurio.TabIndex = 0;
            // 
            // tmrON
            // 
            this.tmrON.Tick += new System.EventHandler(this.tmrON_Tick);
            // 
            // tmrOFF
            // 
            this.tmrOFF.Tick += new System.EventHandler(this.tmrOFF_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 257);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Temperatura (°C):";
            // 
            // nudTemperatura
            // 
            this.nudTemperatura.Location = new System.Drawing.Point(102, 254);
            this.nudTemperatura.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.nudTemperatura.Minimum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.nudTemperatura.Name = "nudTemperatura";
            this.nudTemperatura.Size = new System.Drawing.Size(50, 20);
            this.nudTemperatura.TabIndex = 5;
            this.nudTemperatura.Value = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this.nudTemperatura.ValueChanged += new System.EventHandler(this.nudTemperatura_ValueChanged);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.pictureBox3.Location = new System.Drawing.Point(234, 193);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(27, 13);
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Lime;
            this.pictureBox1.Location = new System.Drawing.Point(234, 247);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 13);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.pictureBox4.Location = new System.Drawing.Point(234, 166);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(27, 13);
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.pictureBox2.Location = new System.Drawing.Point(234, 220);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 13);
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.pictureBox5.Location = new System.Drawing.Point(234, 139);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(27, 13);
            this.pictureBox5.TabIndex = 6;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.pictureBox6.Location = new System.Drawing.Point(234, 112);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(27, 13);
            this.pictureBox6.TabIndex = 6;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.pictureBox7.Location = new System.Drawing.Point(234, 85);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(27, 13);
            this.pictureBox7.TabIndex = 6;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.pictureBox8.Location = new System.Drawing.Point(234, 58);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(27, 13);
            this.pictureBox8.TabIndex = 6;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.pictureBox9.Location = new System.Drawing.Point(234, 31);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(27, 13);
            this.pictureBox9.TabIndex = 6;
            this.pictureBox9.TabStop = false;
            // 
            // ucHorno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.nudTemperatura);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panelTermómetro);
            this.Controls.Add(this.pbxTermómetro);
            this.Controls.Add(this.btnOFF);
            this.Controls.Add(this.btnON);
            this.Controls.Add(this.pbxHornoON);
            this.Controls.Add(this.pbxHornoApagado);
            this.Name = "ucHorno";
            this.Size = new System.Drawing.Size(285, 303);
            this.Load += new System.EventHandler(this.ucHorno_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoApagado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxTermómetro)).EndInit();
            this.panelTermómetro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudTemperatura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxHornoApagado;
        private System.Windows.Forms.PictureBox pbxHornoON;
        private System.Windows.Forms.Button btnON;
        private System.Windows.Forms.Button btnOFF;
        private System.Windows.Forms.PictureBox pbxTermómetro;
        private System.Windows.Forms.Panel panelTermómetro;
        private System.Windows.Forms.FlowLayoutPanel panelMercurio;
        private System.Windows.Forms.Timer tmrON;
        private System.Windows.Forms.Timer tmrOFF;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudTemperatura;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
    }
}
