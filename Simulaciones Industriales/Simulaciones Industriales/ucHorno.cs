﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Simulaciones_Industriales
{
    public partial class ucHorno : UserControl
    {
        public ucHorno()
        {
            InitializeComponent();
        }

        private void nudTemperatura_ValueChanged(object sender, EventArgs e)
        {
            panelMercurio.Height = (int) (nudTemperatura.Value - 26) * panelTermómetro.Height / (180 - 26);

            // Versión larga de IF-ELSE con Llaves
            if (nudTemperatura.Value >= 40)
            {
                pictureBox2.BackColor = Color.Lime;
            }
            else
            {
                pictureBox2.BackColor = Color.FromArgb(0, 32, 0);
            }

            // Versión abreviada de IF-ELSE sin Llaves
            // para cuando sólo hay una instrucción dentro
            if (nudTemperatura.Value >= 60) 
                pictureBox3.BackColor = Color.Lime;
            else 
                pictureBox3.BackColor = Color.FromArgb(0, 32, 0);

            if (nudTemperatura.Value >= 80)
                pictureBox4.BackColor = Color.Lime;
            else
                pictureBox4.BackColor = Color.FromArgb(0, 32, 0);

            if (nudTemperatura.Value >= 100)
                pictureBox5.BackColor = Color.Lime;
            else
                pictureBox5.BackColor = Color.FromArgb(0, 32, 0);

            if (nudTemperatura.Value >= 120)
                pictureBox6.BackColor = Color.Lime;
            else
                pictureBox6.BackColor = Color.FromArgb(0, 32, 0);

            if (nudTemperatura.Value >= 140)
                pictureBox7.BackColor = Color.Lime;
            else
                pictureBox7.BackColor = Color.FromArgb(0, 32, 0);

            if (nudTemperatura.Value >= 160)
                pictureBox8.BackColor = Color.Lime;
            else
                pictureBox8.BackColor = Color.FromArgb(0, 32, 0);

            if (nudTemperatura.Value >= 180)
                pictureBox9.BackColor = Color.Lime;
            else
                pictureBox9.BackColor = Color.FromArgb(0, 32, 0);
        }

        private void btnON_Click(object sender, EventArgs e)
        {
            tmrON.Enabled = true;
            tmrOFF.Enabled = false;
            pbxHornoON.Visible = true;
        }

        private void btnOFF_Click(object sender, EventArgs e)
        {
            tmrON.Enabled = false;
            tmrOFF.Enabled = true;
            pbxHornoON.Visible = false;
        }

        private void tmrON_Tick(object sender, EventArgs e)
        {
            if (nudTemperatura.Value < 180)
            {
                nudTemperatura.Value++;
            }
        }

        private void tmrOFF_Tick(object sender, EventArgs e)
        {
            if (nudTemperatura.Value > 26)
            {
                nudTemperatura.Value--;
            }
        }

        private void ucHorno_Load(object sender, EventArgs e)
        {
            nudTemperatura.Value = 26;
        }
    }
}
